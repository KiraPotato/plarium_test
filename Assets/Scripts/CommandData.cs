﻿using UnityEngine;

namespace TestThing
{
    public struct CommandData
    {
        public CommandType CommandType;
        public string ShapeName;
        public ApplyCommandType ApplyCommandType;
        public object Extra;

        public bool TryCastExtra<TDataType>(out TDataType result)
        {
            if (Extra is TDataType cast)
            {
                result = cast;
                return true;
            }

            result = default;
            return false;
        }
    }
}