﻿using System.Collections.Generic;
using UnityEngine;

namespace TestThing
{
    public class EntityManager : System.IDisposable
    {
        private Dictionary<string, Entity> createdObjects;
        private Queue<Entity> disabledObjects;
        private GameObject prefab;
        private MeshCollection meshCollection;
        private ColorCollection colorCollection;

        public EntityManager(GameObject prefab)
        {
            createdObjects = new Dictionary<string, Entity>();
            disabledObjects = new Queue<Entity>();
            meshCollection = new MeshCollection();
            colorCollection = new ColorCollection();

            this.prefab = prefab;
        }

        public bool TryRunCommand(CommandData data)
        {
            if (data.CommandType == CommandType.ERROR || string.IsNullOrWhiteSpace(data.ShapeName))
            {
                Log.Error("Trying to run invalid command");
                return false;
            }

            switch (data.CommandType)
            {
                case CommandType.CREATE:
                    return TryCreate(data);

                case CommandType.MOVE:
                    return TryMove(data);

                case CommandType.SCALE:
                    return TryScale(data);

                case CommandType.DESTROY:
                    return TryDestroy(data);

                case CommandType.APPLY:
                    return TryApply(data);

                default:
                    Log.Error("Unsupported command " + data.CommandType);
                    return false;
            }
        }

        private bool TryCreate(CommandData data)
        {
            if (createdObjects.ContainsKey(data.ShapeName))
            {
                Log.Error("Cant create duplicate shape name: " + data.ShapeName);
                return false;
            }

            if (data.TryCastExtra(out Shape shape) == false || shape == Shape.Error)
            {
                Log.Error("Bad shape recieved on creation!" + shape.ToString());
                return false;
            }

            if (meshCollection.TryGetShape(shape, out Mesh mesh) == false)
            {
                Log.Error("Failed to get shape for type " + shape.ToString());
                return false;
            }

            Color? color = null;

            if (colorCollection.TryGetColor(shape, out Color fetchedColor))
                color = fetchedColor;

            Entity entity = GetCleanEntity(data, mesh, color);

            createdObjects[data.ShapeName] = entity;
            return true;
        }

        private bool TryMove(CommandData data)
        {
            if (createdObjects.TryGetValue(data.ShapeName, out Entity shape) == false)
            {
                Log.Error("Trying to move shape that doesnt exist " + data.ShapeName);
                return false;
            }

            if (data.TryCastExtra(out Vector3 direction) == false)
            {
                Log.Error("Invalid data for movement.");
                return false;
            }

            return shape.TryMove(direction);
        }

        private bool TryScale(CommandData data)
        {
            if (createdObjects.TryGetValue(data.ShapeName, out Entity shape) == false)
            {
                Log.Error("Trying to scale shape that doesnt exist " + data.ShapeName);
                return false;
            }

            if (data.TryCastExtra(out Vector3 multiplier) == false)
            {
                Log.Error("Invalid data for scale.");
                return false;
            }

            return shape.TryScale(multiplier);
        }

        private bool TryDestroy(CommandData data)
        {
            if (createdObjects.TryGetValue(data.ShapeName, out Entity shape) == false)
            {
                Log.Error("Trying to destroy shape that doesnt exist " + data.ShapeName);
                return false;
            }

            createdObjects.Remove(data.ShapeName);

            if (shape.TryDisable())
                disabledObjects.Enqueue(shape);
            else
                shape.Dispose();

            return true;
        }

        private bool TryApply(CommandData data)
        {
            switch (data.ApplyCommandType)
            {
                case ApplyCommandType.TEXTURE:
                    return TryApplyTexture(data);

                case ApplyCommandType.COLOR:
                    return TryApplyColor(data);

                default:
                    Log.Error("Unsupported APPLY command with suffix " + data.ApplyCommandType);
                    return false;
            }
        }

        private bool TryApplyTexture(CommandData data)
        {
            if (createdObjects.TryGetValue(data.ShapeName, out Entity shape) == false)
            {
                Log.Error("Trying to manipulate shape that doesnt exist " + data.ShapeName);
                return false;
            }

            if (data.TryCastExtra(out string path) == false)
            {
                Log.Error("Invalid data for APPLY TEXTURE.");
                return false;
            }

            if (TextureLoader.TryLoadTexture(path, out Texture texture) == false)
            {
                Log.Error("Failed to load texture from path: " + path);
                return false;
            }

            return shape.TryApplyTexture(texture);
        }

        private bool TryApplyColor(CommandData data)
        {
            if (createdObjects.TryGetValue(data.ShapeName, out Entity shape) == false)
            {
                Log.Error("Trying to manipulate shape that doesnt exist " + data.ShapeName);
                return false;
            }

            if (data.TryCastExtra(out Color color) == false)
            {
                Log.Error("Invalid data for APPLY COLOR.");
                return false;
            }

            return shape.TryApplyColor(color);
        }

        private Entity GetCleanEntity(CommandData data, Mesh mesh, Color? color)
        {
            Entity entity;

            if (disabledObjects.Count > 0)
            {
                entity = disabledObjects.Dequeue();

                if (entity.TryEnable(data.ShapeName, mesh, color) == false)
                {
                    entity.Dispose();

                    entity = new Entity(data.ShapeName, mesh, prefab, color);
                }
            }
            else
                entity = new Entity(data.ShapeName, mesh, prefab, color);

            return entity;
        }

        public void Dispose()
        {
            foreach (var item in createdObjects.Values)
            {
                item.Dispose();
            }

            createdObjects.Clear();

            while (disabledObjects.Count > 0)
            {
                disabledObjects.Dequeue().Dispose();
            }
        }
    }
}