﻿using System.Collections.Generic;
using UnityEngine;

namespace TestThing
{
    public class MeshCollection
    {
        private readonly Dictionary<Shape, Mesh> meshes;

        public MeshCollection()
        {
            meshes = new Dictionary<Shape, Mesh>(2);

            meshes[Shape.Triangle] = CreateTriangle(Vector3.one * 10f);
            meshes[Shape.Square] = CreateSquare(Vector3.one * 10f);
            meshes[Shape.Circle] = CreateCircle(10f, 10f, 50);
        }

        public bool TryGetShape(Shape shapeType, out Mesh mesh)
        {
            if (meshes.TryGetValue(shapeType, out mesh))
                return true;

            mesh = default;
            Log.Error("Unsupported shape of type: " + shapeType.ToString());
            return false;
        }

        private static Mesh CreateTriangle(Vector3 size)
        {
            float width = size.x * .5f;
            float height = size.x * .5f;
            float z = size.z;

            var mesh = new Mesh();
            var verticies = new Vector3[3];
            var triangles = new int[3];
            var normals = new Vector3[3];
            var uv = new Vector2[3];

            verticies[0] = new Vector3(width, height, z); // top right
            verticies[1] = new Vector3(0f, -height, z); // bottom center
            verticies[2] = new Vector3(-width, height, z); // top left

            triangles[0] = 0;
            triangles[1] = 1;
            triangles[2] = 2;

            normals[0] = -Vector3.forward;
            normals[1] = -Vector3.forward;
            normals[2] = -Vector3.forward;

            uv[0] = new Vector2(1f, 1f);
            uv[1] = new Vector2(.5f, 0f);
            uv[2] = new Vector2(0f, 1f);

            mesh.vertices = verticies;
            mesh.triangles = triangles;
            mesh.normals = normals;
            mesh.uv = uv;

            return mesh;
        }

        private static Mesh CreateSquare(Vector3 size)
        {
            float width = size.x * .5f;
            float height = size.x * .5f;
            float z = size.z;

            var mesh = new Mesh();
            var verticies = new Vector3[4];
            var tri = new int[6];
            var normals = new Vector3[4];
            var uv = new Vector2[4];

            verticies[0] = new Vector3(-width, -height, z); // bottom left
            verticies[1] = new Vector3(-width, height, z); // top left
            verticies[2] = new Vector3(width, height, z); // top right
            verticies[3] = new Vector3(width, -height, z); // bottom right

            tri[0] = 0;
            tri[1] = 1;
            tri[2] = 3;

            tri[3] = 1;
            tri[4] = 2;
            tri[5] = 3;

            normals[0] = -Vector3.forward;
            normals[1] = -Vector3.forward;
            normals[2] = -Vector3.forward;
            normals[3] = -Vector3.forward;

            uv[0] = new Vector2(0f, 0f);
            uv[1] = new Vector2(0f, 1f);
            uv[2] = new Vector2(1f, 1f);
            uv[3] = new Vector2(1f, 0f);

            mesh.vertices = verticies;
            mesh.triangles = tri;
            mesh.normals = normals;
            mesh.uv = uv;

            return mesh;
        }

        private static Mesh CreateCircle(float radius, float z, int edges = 10)
        {
            var mesh = new Mesh();

            radius *= .5f;

            var verticies = new Vector3[edges];
            var uvs = new Vector2[edges];
            var normals = new Vector3[edges];
            var triangles = new int[(edges - 2) * 3];

            float x, y;

            for (int i = 0; i < edges; i++)
            {
                x = radius * Mathf.Sin((2 * Mathf.PI * i) / edges);
                y = radius * Mathf.Cos((2 * Mathf.PI * i) / edges);
                verticies[i] = new Vector3(x, y, z);
            }

            for (int i = 0; i < (edges - 2); i++)
            {
                triangles[i * 3] = 0;
                triangles[i * 3 + 1] = i + 1;
                triangles[i * 3 + 2] = i + 2;
            }

            for (int i = 0; i < edges; i++)
            {
                normals[i] = -Vector3.forward;
            }

            for (int i = 0; i < uvs.Length; i++)
            {
                uvs[i] = new Vector2(verticies[i].x / (radius * 2) + 0.5f, verticies[i].y / (radius * 2) + 0.5f);
            }

            mesh.vertices = verticies;
            mesh.triangles = triangles;
            mesh.normals = normals;
            mesh.uv = uvs;

            return mesh;
        }
    }
}