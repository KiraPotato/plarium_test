﻿namespace TestThing
{
    public enum CommandType
    {
        ERROR,
        CREATE,
        MOVE,
        SCALE,
        DESTROY,
        APPLY,
    }
}