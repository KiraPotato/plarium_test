﻿using System.Collections.Generic;
using UnityEngine;

namespace TestThing
{
    public static class CommandFactory
    {
        private delegate bool CommandMethodDelegate(ref CommandData commandData, string[] args);

        private static Dictionary<CommandType, CommandMethodDelegate> commands = new Dictionary<CommandType, CommandMethodDelegate>
        {
            { CommandType.CREATE, TryMakeCreateData },
            { CommandType.MOVE, TryMakeMoveData },
            { CommandType.SCALE, TryMakeScaleData },
            { CommandType.DESTROY, TryMakeDestroyData },
            { CommandType.APPLY, TryMakeApplyCommand },
        };

        public static bool TryCreateCommandData(string input, out CommandData commandData)
        {
            commandData = default;

            try
            {
                string[] cut = input.Split(' ');

                commandData = new CommandData();

                if (System.Enum.TryParse(cut[0], ignoreCase: true, out commandData.CommandType) == false)
                    return false;

                if (commands.TryGetValue(commandData.CommandType, out var commandGenerationMethod))
                {
                    return commandGenerationMethod(ref commandData, cut);
                }
                else
                {
                    Log.Error(string.Format("Failed to run command for type {0} from input {1}", commandData.CommandType, input));
                    return false;
                }
            }
            catch (System.Exception)
            {
                return false;
            }
        }

        private static bool TryMakeCreateData(ref CommandData data, string[] args)
        {
            var shapeType = args[1];
            var shapeName = args[2];

            if (System.Enum.TryParse(shapeType, ignoreCase: true, out Shape shape) == false)
                return false;

            data.ShapeName = shapeName;
            data.Extra = shape;

            return true;
        }

        private static bool TryMakeMoveData(ref CommandData data, string[] args)
        {
            string shapeName = args[1];
            string amountString = args[2];
            string directionString = args[3];

            if (TryTranslateTextToDirection(amountString, directionString, out Vector3 direction) == false)
                return false;

            data.ShapeName = shapeName;
            data.Extra = direction;
            return true;
        }

        private static bool TryMakeScaleData(ref CommandData data, string[] args)
        {
            string shapeName = args[1];
            string scaleString = args[2];

            if (float.TryParse(scaleString, out float scale) == false)
                return false;

            data.ShapeName = shapeName;
            data.Extra = Vector3.one * scale;
            return true;
        }

        private static bool TryMakeDestroyData(ref CommandData data, string[] args)
        {
            string shapeName = args[1];

            data.ShapeName = shapeName;
            return true;
        }

        private static bool TryMakeApplyCommand(ref CommandData data, string[] args)
        {
            if (args[3].Equals("to", System.StringComparison.OrdinalIgnoreCase))
            {
                return TryGetTargetedApplyCommand(ref data, args);
            }

            Log.Error("Unknown operator after command. Example usage APPLY <INFO> <DATA> to <NAME>");
            return false;
        }

        private static bool TryGetTargetedApplyCommand(ref CommandData data, string[] args)
        {
            string appliedType = args[1];
            string appliedData = args[2];
            string shapeName = args[4];

            if (System.Enum.TryParse(appliedType, true, out data.ApplyCommandType) == false)
            {
                Log.Error("Bad application parameter for APPLY X function: " + appliedType);
                return false;
            }

            if (string.IsNullOrWhiteSpace(appliedData))
            {
                Log.Error("Applied data cannot be empty!");
                return false;
            }

            switch (data.ApplyCommandType)
            {
                case ApplyCommandType.TEXTURE:
                    data.Extra = appliedData;
                    break;

                case ApplyCommandType.COLOR:
                    if (TryGetColorFromString(appliedData, out Color color))
                        data.Extra = color;
                    else
                        return false;
                    break;

                default:
                    Log.Error("Unsupported APPLY type: " + data.ApplyCommandType);
                    return false;
            }

            data.ShapeName = shapeName;
            return true;
        }

        private static bool TryGetColorFromString(string colorString, out Color color)
        {
            try
            {
                var split = colorString.Split(',');

                color = Color.black;

                color.r = float.Parse(split[0]);
                color.g = float.Parse(split[1]);
                color.b = float.Parse(split[2]);

                if (split.Length > 3)
                    color.a = float.Parse(split[3]);

                return true;
            }
            catch (System.Exception)
            {
                Log.Error("Failed to parse color string: " + colorString);
                color = default;
                return false;
            }
        }

        private static bool TryTranslateTextToDirection(string amountString, string direction, out Vector3 result)
        {
            result = Vector3.zero;

            if (float.TryParse(amountString, out float ammount) == false)
                return false;

            if (direction.Equals("X", System.StringComparison.OrdinalIgnoreCase))
                result.x = ammount;
            else if (direction.Equals("Y", System.StringComparison.OrdinalIgnoreCase))
                result.y = ammount;
            else if (direction.Equals("Z", System.StringComparison.OrdinalIgnoreCase))
                result.z = ammount;
            else
                return false;

            return true;
        }
    }
}