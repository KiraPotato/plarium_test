﻿using System.Collections.Generic;
using UnityEngine;

namespace TestThing
{
    public class ColorCollection
    {
        private readonly Dictionary<Shape, Color> colors = new Dictionary<Shape, Color>
        {
            { Shape.Square, Color.blue },
            { Shape.Triangle, Color.green },
            { Shape.Circle, Color.red },
        };

        public bool TryGetColor(Shape shape, out Color color)
        {
            if (colors.TryGetValue(shape, out color))
                return true;

            Log.Error("No color set for shape " + shape);
            return false;
        }
    }
}