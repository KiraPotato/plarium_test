﻿using UnityEngine;

namespace TestThing
{
    public class Entity : System.IDisposable
    {
        private readonly GameObject gameObject;
        private readonly MeshFilter filter;
        private readonly MeshRenderer renderer;
        private readonly Vector3 originalPosition;
        private readonly Vector3 originalScale;
        private readonly Color originalColor;
        private readonly Texture originalTexture;

        public Entity(string name, Mesh mesh, GameObject prefab, Color? color = null)
        {
            if (prefab == null)
                return;

            gameObject = GameObject.Instantiate(prefab);
            filter = gameObject.GetComponent<MeshFilter>();
            renderer = gameObject.GetComponent<MeshRenderer>();

            if (filter == null || renderer == null)
            {
                Log.Error("Missing MeshFilter or MeshRenderer component on prefab!", prefab);
                return;
            }

            originalPosition = gameObject.transform.position;
            originalScale = gameObject.transform.localScale;
            originalColor = renderer.sharedMaterial.color;
            originalTexture = renderer.sharedMaterial.mainTexture;

            TryUpdateData(name, mesh, color);
        }


        public bool TryEnable(string name, Mesh mesh, Color? color = null)
        {
            if (gameObject == null)
                return false;

            gameObject.transform.position = originalPosition;
            gameObject.transform.localScale = originalScale;

            if (renderer != null)
            {
                renderer.material.mainTexture = originalTexture;

                if (color == null)
                    renderer.material.color = originalColor;
            }

            gameObject.SetActive(true);

            return TryUpdateData(name, mesh, color);
        }

        public bool TryDisable()
        {
            if (gameObject == null)
                return false;

            gameObject.SetActive(false);

            return true;
        }

        public bool TryMove(Vector3 direction)
        {
            if (gameObject == null)
                return false;

            gameObject.transform.position += direction;
            return true;
        }

        public bool TryScale(Vector3 multiplier)
        {
            if (gameObject == null)
                return false;

            var currScale = gameObject.transform.localScale;

            currScale.x *= multiplier.x;
            currScale.y *= multiplier.y;
            currScale.z *= multiplier.z;

            gameObject.transform.localScale = currScale;

            return true;
        }

        public bool TryApplyColor(Color newColor)
        {
            if (renderer == null)
                return false;

            renderer.material.color = newColor;
            return true;
        }

        public bool TryApplyTexture(Texture texture)
        {
            if (renderer == null)
                return false;

            renderer.material.mainTexture = texture;
            return true;
        }

        private bool TryUpdateData(string name, Mesh mesh, Color? color = null)
        {
            if (gameObject == null || filter == null)
                return false;

            if (renderer != null && color != null)
                renderer.material.color = (Color)color;

            gameObject.name = name;
            filter.mesh = mesh;
            return true;
        }

        public void Dispose()
        {
            if (gameObject != null)
                Object.Destroy(gameObject);
        }
    }
}