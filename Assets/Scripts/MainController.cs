﻿using UnityEngine;
using UnityEngine.UI;

namespace TestThing
{
    public class MainController : MonoBehaviour
    {
#pragma warning disable 649
        [SerializeField] private GameObject prefab;

        [Header("Scene References")]
        [SerializeField] private InputField inputField;
        [SerializeField] private Button button;

        [Header("Debug")]
        [SerializeField] private string[] commandsOnStart;
#pragma warning restore 649

        private EntityManager shapes;

        private void Awake()
        {
            Log.Initialize();

            if (prefab == null || inputField == null || button == null)
            {
                Log.Error("One of the scene references is missing!", this);
                return;
            }

            shapes = new EntityManager(prefab);
            button.onClick.AddListener(OnRunCommand);
        }

        private void Start()
        {
            if (commandsOnStart != null)
            {
                foreach (var command in commandsOnStart)
                {
                    RunCommand(command);
                }
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                OnRunCommand();
                inputField.ActivateInputField();
            }

            if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.Backspace))
            {
                if (inputField != null)
                {
                    inputField.text = string.Empty;
                    inputField.ActivateInputField();
                }
            }
        }

        private void OnDestroy()
        {
            if (shapes != null)
                shapes.Dispose();

            if (button != null)
                button.onClick.RemoveAllListeners();
        }

        private void OnRunCommand()
        {
            if (inputField == null)
                return;

            var input = inputField.text;
            RunCommand(input);
        }

        private void RunCommand(string input)
        {
            if (CommandFactory.TryCreateCommandData(input, out CommandData data))
            {
                if (shapes.TryRunCommand(data))
                    Log.Message("Command completed successfully: " + input);
                else
                    Log.Error("Failed to run command for input: " + input);
            }
            else
            {
                Log.Error("Failed to parse command: " + input);
            }
        }
    }
}