﻿using System.IO;
using UnityEngine;

namespace TestThing
{
    public static class TextureLoader
    {
        public static bool TryLoadTexture(string path, out Texture texture)
        {
            Texture2D tex = null;

            try
            {
                if (File.Exists(path))
                {
                    var data = File.ReadAllBytes(path);
                    tex = new Texture2D(0, 0);
                    tex.LoadImage(data);
                }
            }
            catch (System.Exception)
            {
                Log.Error("Failed to load texture from path " + path);
            }

            texture = tex;
            return texture != null;
        }
    }
}