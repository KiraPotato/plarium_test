﻿using System.Collections.Concurrent;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

using UnityEngine;

namespace TestThing
{
    public static class Log
    {
        private static ConcurrentQueue<string> writtenLines = new ConcurrentQueue<string>();

        private static string targetFilePath;
        private static CancellationTokenSource fileWritingTokenSource;

        public static void Initialize()
        {
            var now = System.DateTime.Now;
            var fileName = string.Format("{0}.txt", now.ToString("dd-MM-yyyy hh-mm-ss-ffff"));
            var folderLocation = Path.Combine(Application.persistentDataPath, "Shape Builder Logs");
            var directory = Directory.CreateDirectory(folderLocation);

            if (fileWritingTokenSource != null)
                fileWritingTokenSource.Cancel();

            fileWritingTokenSource = new CancellationTokenSource();

            targetFilePath = Path.GetFullPath(Path.Combine(directory.FullName, fileName));

            Task.Run(FileWriting, fileWritingTokenSource.Token);

            Message("Logger initialized at path " + targetFilePath);
        }

        public static void Message(string message, Object context = null)
            => DoLog(message, context, Debug.Log, "Message");

        public static void Error(string message, Object context = null)
            => DoLog(message, context, Debug.LogError, "Error");

        private static void DoLog(string message, Object context, System.Action<string, Object> engineLogMethod, in string logType)
        {
            message = string.Format("[{1}] [{0}] - {2}", System.DateTime.Now.ToString("HH:mm:ss:ffff"), logType, message);

            writtenLines.Enqueue(message);

            engineLogMethod(message, context);
        }

        private static async Task FileWriting()
        {
            var cancelToken = fileWritingTokenSource.Token;
            var logPath = targetFilePath;

            while (true)
            {
                await Task.Delay(100);

                if (cancelToken.IsCancellationRequested)
                    return;

                if (writtenLines.Count == 0)
                    continue;

                using (var stream = File.AppendText(logPath))
                {
                    while (writtenLines.TryDequeue(out string line))
                    {
                        if (cancelToken.IsCancellationRequested)
                            return;

                        await stream.WriteLineAsync(line);
                    }
                }
            }
        }
    }
}